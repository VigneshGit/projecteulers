package main

import "fmt"

type Account struct {
	acctName string
	acctType string
	balance  string
}

func main() {
	fmt.Println("Welcome to Go World")

	// var variable_name variable_type                          => for variables
	// var variable_name [size ] variable_name variable_type    => for array
	// var variable_name *variable_type                         => for pointer variable

	// Static type Declaration
	var age uint8
	var balance int32
	var acctNumber string

	fmt.Printf("The data type of age %T\n", age)
	fmt.Printf("The data type of balance %T\n", balance)
	fmt.Printf("The data type of acctNumber %T\n", acctNumber)

	// Dynamic Type Declaration
	var x float64 = 80.0
	y := 25

	fmt.Printf("The data type of x %T\n", x)
	fmt.Printf("The data type of y %T\n", y)

	// Mixed Type Declaration
	var a, b, c = 3, 4, "foo"
	fmt.Printf("The data type of a %T\n", a)
	fmt.Printf("The data type of b %T\n", b)
	fmt.Printf("The data type of c %T\n", c)

	/**
	  func function_name([parameter list]) [return_types]{
	    body of the function
	  }
	**/

	sum := add(10, 20)
	fmt.Println("The sum value is:", sum)

	firstName, lastName := swap("Vignesh", "Diya")
	fmt.Println("The swapped value is:", firstName, lastName)

	greeting := "Hello World"
	for i := 0; i < len(greeting); i++ {
		fmt.Printf("%x", greeting[i])
	}

	var p int = 10
	var op int  // 0
	var ip *int // nil

	fmt.Println("\n**The value of p is %d", p)
	fmt.Println("**The value of op is %d", op) // 0
	fmt.Println("**The value of ip is %d", ip) // nil

	ip = &p
	fmt.Println("**The value of ip is %x", ip) // address of p in pointer variable

	/** Structure Defenition
	   type struct_variable_type struct {
	       member definition;
	       member definition;
	   }
	**/

	// Syntax
	// variable_name := structure_variable_type {value1, value2...valuen}

	type Car struct {
		name  string
		model string
		price string
	}

	// Declare VW car of Car
	var car1 Car
	var car2 Car

	car1.name = "VW"
	car1.model = "Golf VIII"
	car1.price = "40000 Euros"

	car2.name = "AUDI"
	car2.model = "Q3 Sportback"
	car2.price = "22000 Euros"

	fmt.Printf("Car 1 name : %s\n", car1.name)
	fmt.Printf("Car 1 model : %s\n", car1.model)
	fmt.Printf("Car 2 price : %s\n", car2.price)

	// Structures as pointers
	/** Structure Defenition
	   type *struct_variable_type struct {
	       member definition;
	       member definition;
	   }
	**/

	// Syntax
	// variable_name := *structure_variable_type {value1, value2...valuen}

	var account1 Account
	account1.acctName = "Vignesh"
	account1.acctType = "Savings"
	account1.balance = "10 Euros"

	printAccount(&account1)

	ru := 'a'
	fmt.Println("ru: %V %T", ru, ru)

	sa := "a"
	fmt.Println("sa: %V %T", sa, sa)
}

func printAccount(account *Account) {
	fmt.Printf("Account name : %s\n", account.acctName)
	fmt.Printf("Account type : %s\n", account.acctType)
	fmt.Printf("Account balance : %s\n", account.balance)
}

func add(a, b int) int {
	return a + b
}

// Method returns multiple values
func swap(x, y string) (string, string) {
	return y, x
}
