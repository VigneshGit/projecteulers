package main

import "fmt"

func main()  {
  //++++++++++++++++BubbleSort++++++++++++++++//
   var array []int = []int{6,3,8,0,7,18,52}
   fmt.Println("The given array is: %v", array)

   for i := 0; i < len(array) - 1 ; i++ {
    for j := 0; j < len(array) - 1 - i; j++ {
      if array[j] > array[j+1] {
         tmp := array[j]
         array[j] = array[j+1]
         array[j+1] = tmp
      }
    }
   }

   fmt.Println("The BubbleSorted array is: %v", array)

 //++++++++++++++++SelectionSort++++++++++++++++//
 fmt.Println("================================================")

   var arr []int = []int{16,23,38,1,17,18,52,7}
   fmt.Println("The given array is: %v", arr)

   minValue := arr[0]
   for i := 0; i < len(arr) - 1; i++ {
      minIndex := i
      for j := i; j < len(arr) - 1; j++ {
        if arr[minIndex] > arr[j+1] {
            minValue = arr[j+1]
            minIndex = j+1
        }
        if j == len(arr) - 2 {
           tmpVal := arr[i]
           arr[i] = minValue
           arr[minIndex] = tmpVal
        }
      }
   }
   fmt.Println("The SelectionSorted array is: %v", arr)
}
