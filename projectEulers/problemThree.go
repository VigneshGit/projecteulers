package main

import "fmt"

func main() {

  // Problem 3: Largest prime factor
  /**
    The prime factors of 13195 are 5, 7, 13 and 29.
    What is the largest prime factor of the number 600851475143 ?
  **/

  var largestPrimeFactor int = getLargestPrimeFactor()
  fmt.Println("The largest prime factor of the number 600851475143 %v", largestPrimeFactor)
}


 func getLargestPrimeFactor() int {
    givenNumber := 600851475143
    initValue := 2
    resultValue := 0
    for {
       if givenNumber % initValue == 0 {
          quotient := givenNumber / initValue
          givenNumber = quotient
          if quotient == 1 {
              resultValue = initValue
              break
          }
          initValue = 2
       } else {
         initValue += 1
       }
    }
    return resultValue
 }
