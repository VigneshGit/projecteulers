package main

import (
	"fmt"
	"math"
)

type Shape interface {
	area() float32
}

type Circle struct {
	radius float32
}

type Rectangle struct {
	length float32
	breath float32
}

func (circle Circle) area() float32 {
	return math.Pi * circle.radius * circle.radius
}

func (rect Rectangle) area() float32 {
	return rect.length * rect.breath
}

func getArea(shape Shape) float32 {
	return shape.area()
}

func main() {
	circle := Circle{radius: 5}
	rectangle := Rectangle{length: 5, breath: 5}

	fmt.Println("The area of Circle is %f:", getArea(circle))
	fmt.Println("The area of Rectangle is %f:", getArea(rectangle))
}
