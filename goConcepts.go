package main

import (
	"fmt"
	"math"
)

type Rectangle struct {
	length, breath float32
}

type Circle struct {
	radius float32
}

func main() {
	//map[type]type
	testMap := map[string]int{"A": 10, "B": 5}
	fmt.Println("Print Map=====: ", testMap)

	//Area map[type]type
	rectArea := make(map[string]Rectangle)
	rectArea["Rectangle"] = Rectangle{length: 20, breath: 10}
	fmt.Println("Print rectArea=====: ", rectArea)

	circleArea := make(map[string]Circle)
	circleArea["Circle"] = Circle{radius: 5}
	fmt.Println("Print circleArea=====: ", circleArea)

	areaC := Circle{radius: 5}
	areaC.findCircleArea()

	areaRect := Rectangle{length: 5, breath: 5}
	areaRect.findRectangleArea()
}

func (rect Rectangle) findRectangleArea() {
	rectArea := rect.length * rect.breath
	fmt.Println("Print rectangleArea=====: ", rectArea)
}

func (circle Circle) findCircleArea() {
	circleArea := math.Pi * circle.radius * circle.radius
	fmt.Println("Print circleArea=====: ", circleArea)
}
